#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
int count=0;
pthread_mutex_t mutex;
void *start_thread_1(void *arg)
{
	int i;
	pthread_mutex_lock(&mutex);
	printf("Thread 1 \n");
	for(i=0;i<10;i++) 
		{
			count+=1;
			printf("gia tri counter: %d \n",count);
			printf("arg: %08x \n",&arg);
			usleep(100000);
		}
	pthread_mutex_unlock(&mutex);
}

void *start_thread_2(void *arg)
{
	int i;
	pthread_mutex_lock(&mutex);
	printf("Thread 2 \n");
	for(i=0;i<20;i++)
		{
			count+=1;
			printf("gia tri counter: %d \n",count);
			printf("arg: %08x \n",&arg);
			usleep(100000);
		}
	pthread_mutex_unlock(&mutex);
}

int main (int argc,char *argv[])
{
	unsigned int arg;
	int rc, rc1, rc2;
	pthread_mutex_init(&mutex,NULL);
	pthread_t thread1;
	pthread_t thread2;
	if((rc = fork()) ==0 )
	{
		printf("This is child process and my pid is : %d \n",getpid());
		execl("/bin/ls","ls","-a",NULL);
		printf("Could not access /bin/ls \n");
		exit(0);
	}
	else if(rc>0)
	{
		printf("This is parent process and my pid is :%d \n",getppid());
		printf("Child process pid is: %d \n",rc);
		rc1 = pthread_create(&thread1,NULL,&start_thread_1,(void*) &arg);
		printf("rc1: %d \n",rc1);
		if(rc1!=0)
		{
			printf("fail to create thread");
		}
		usleep(100000);
		rc2 = pthread_create(&thread2,NULL,&start_thread_2,(void*) &arg);
		printf("rc2: %d \n",rc2);
		if(rc2!=0)
		{
			printf("fail to create thread");
		}
		//sleep(1);
		pthread_join(thread1,NULL);
		pthread_join(thread2,NULL);
		exit(0);
	}
	else 
	{
		printf("Error create process");
		exit(0);
	}
	return 0;
}
